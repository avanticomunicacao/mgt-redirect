<?php
/**
 * Avanti Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Redirect Magento 2
 * @category    Avanti
 * @package     Avanti_Redirect
 *
 * @copyright   Copyright (c) 2019 Avanti Soluções Web. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\Redirect\Controller\Index;

use Avanti\Redirect\Helper\Data;
use Magento\Cms\Helper\Page;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Index extends \Magento\Cms\Controller\Noroute\Index
{

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Index constructor.
     * @param Context $context
     * @param ForwardFactory $resultForwardFactory
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $url
     * @param Page $page
     * @param Data $data
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory,
        StoreManagerInterface $storeManager,
        UrlInterface $url,
        Page $page,
        Data $data
    ) {
        parent::__construct($context, $resultForwardFactory);
        $this->storeManager = $storeManager;
        $this->data = $data;
        $this->page = $page;
        $this->url = $url;
    }

    /**
     * @return bool|\Magento\Framework\Controller\Result\Forward|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
	{
		$configValue = $this->data->isEnabled();
		if($configValue)
		{
		    $slug = $this->data->checkCustomUrl($this->url->getCurrentUrl());
		    if ($slug) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$this->storeManager->getStore()->getBaseUrl().$slug);
                exit();
            }
		}
        $pageId = $this->_objectManager->get(
            \Magento\Framework\App\Config\ScopeConfigInterface::class,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )->getValue(
            \Magento\Cms\Helper\Page::XML_PATH_NO_ROUTE_PAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
		$resultPage = $this->page->prepareResultPage($this, $pageId);
		if ($resultPage) {
			$resultPage->setStatusHeader(404, '1.1', 'Not Found');
			$resultPage->setHeader('Status', '404 File not found');
			return $resultPage;
		} else {
			/** @var \Magento\Framework\Controller\Result\Forward $resultForward */
			$resultForward = $this->resultForwardFactory->create();
			$resultForward->setController('index');
			$resultForward->forward('defaultNoRoute');
			return $resultForward;
	    }
    }
}
