<?php
/**
 * Avanti Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Redirect Magento 2
 * @category    Avanti
 * @package     Avanti_Redirect
 *
 * @copyright   Copyright (c) 2019 Avanti Soluções Web. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\Redirect\Helper;

use Avanti\Redirect\Api\CategoryInterface;
use Avanti\Redirect\Api\ProductInterface;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{

    protected $tring = ['/p', 'produto/'];

	protected $_scopeConfig;

	const XML_PATH_AUTOREPLY_ENABLED = 'redirect/general/enable';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ProductUrlPathGenerator
     */
    private $productUrlPathGenerator;
    /**
     * @var ProductInterface
     */
    private $product;
    /**
     * @var CategoryInterface
     */
    private $category;
    /**
     * @var CategoryUrlPathGenerator
     */
    private $categoryUrlPathGenerator;

    /**
     * Data constructor.
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     * @param CategoryUrlPathGenerator $categoryUrlPathGenerator
     * @param ProductInterface $product
     * @param CategoryInterface $category
     */
    public function __construct(
        StoreManagerInterface $storeManager,
	    Context $context,
        ProductUrlPathGenerator $productUrlPathGenerator,
        CategoryUrlPathGenerator $categoryUrlPathGenerator,
        ProductInterface $product,
        CategoryInterface $category
    ) {
		parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->product = $product;
        $this->category = $category;
        $this->categoryUrlPathGenerator = $categoryUrlPathGenerator;
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_AUTOREPLY_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $url
     * @return bool|false|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function checkCustomUrl($url)
    {
        $canonicalUrl = $this->getCanonicalUrl($url);
        $product = $this->product->getProductByUrlKey($canonicalUrl);
        if ($product) {
            return $this->productUrlPathGenerator->getUrlPathWithSuffix(reset($product),$this->storeManager->getStore()->getId());
        }
        $category = $this->category->getCategoryByUrlKey($canonicalUrl);
        if ($category) {
            return $this->categoryUrlPathGenerator->getUrlPathWithSuffix(reset($category),$this->storeManager->getStore()->getId());
        }
        return false;
    }

    /**
     * @param string $string
     * @param $index
     * @param string $delimiter
     * @return string
     */
    public function removeFromString(string $subject, string $search = ''): string
    {
        return str_replace($search, '', $subject) ;
    }

    /**
     * @param $url
     * @return false|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCanonicalUrl($url)
    {
        switch ($url) {
            case strcmp(substr($url,-2),'/p') == 0 :
                return substr($this->removeFromString($url,$this->storeManager->getStore()->getBaseUrl()),0,-2);
                break;
            case strpos($url,'produto/') > 0 :
                return $this->removeFromString($this->removeFromString($url,$this->storeManager->getStore()->getBaseUrl()),'produto/');
                break;
            default:
                return $this->removeFromString($url,$this->storeManager->getStore()->getBaseUrl());
        }
    }
}

