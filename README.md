# module-redirect

Magento 2 module that redirect Vetex url

## installation

- Add the following to the Magento 2 project `composer.json` `repositories` key:
  `"module-redirect": { "url": "https://github.com/penseavanti/module-redirect.git", "type": "git" }`

- Run `composer require penseavanti/module-redirect:dev-master` to add the package to the composer dependencies

## anotations

- To enable: Stores -> Configuration -> Avanti -> 301 Redirect
