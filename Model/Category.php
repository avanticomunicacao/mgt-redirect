<?php
/**
 * Avanti Soluções Web
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Redirect Magento 2
 * @category    Avanti
 * @package     Avanti_Redirect
 *
 * @copyright   Copyright (c) 2019 Avanti Soluções Web. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\Redirect\Model;

use Avanti\Redirect\Api\CategoryInterface;
use Magento\Catalog\Model\CategoryList;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;

class Category implements CategoryInterface
{
    /**
     * @var SearchCriteriaInterface
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var CategoryList
     */
    private $categoryList;


    /**
     * Product constructor.
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param CategoryList $categoryList
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        CategoryList $categoryList
    ) {

        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->categoryList = $categoryList;
    }

    /**
     * @param string $urlKey
     * @return \Magento\Catalog\Api\Data\CategoryInterface[]
     */
    public function getCategoryByUrlKey(string $urlKey)
    {
        $filter = $this->filterBuilder
            ->setField(self::URL_KEY)
            ->setConditionType('like')
            ->setValue('%'.$urlKey.'%')
            ->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        return $this->categoryList->getList($searchCriteria)->getItems();
    }
}